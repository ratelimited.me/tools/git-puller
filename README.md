# Git Puller

The Git Puller component is something hacky written in PHP to initialize a `git pull` in a specific directory.

It is supposed to be used with CI tools if there's no better alternative way.

## But Why

Security requirements made it so we can't hsve SSH users that do not employ 2FA, and there is no safe/secure way to initialize a TOTP login from Gitlab CI.
